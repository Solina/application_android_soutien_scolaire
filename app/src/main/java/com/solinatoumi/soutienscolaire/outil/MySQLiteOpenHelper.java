package com.solinatoumi.soutienscolaire.outil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    //propriete
    private String creationTableUser="CREATE TABLE USER (" +
           // "    user_id INTEGER PRIMARY KEY," +
            "    nom TEXT NOT NULL," +
            "    prenom TEXT NOT NULL," +
            "    email TEXT NOT NULL UNIQUE," +
            "    pw TEXT NOT NULL ," +
            "    role TEXT NOT NULL, " +
            "    niveau NUMBER " +
            ");";


    public MySQLiteOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(creationTableUser);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
