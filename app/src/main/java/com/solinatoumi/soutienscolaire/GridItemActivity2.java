package com.solinatoumi.soutienscolaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class GridItemActivity2 extends AppCompatActivity {
    TextView gridData;
    //ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_item2);

        gridData = findViewById(R.id.griddata2);
       // imageView = findViewById(R.id.imageView2);
        Intent intent = getIntent();
        String receivedName =  intent.getStringExtra("name");
        Log.i("ifon","nom"+receivedName+"******************");
        int receivedImage = intent.getIntExtra("image",0);
        Log.i("ifon","receivedImage"+receivedImage+"******************");




        gridData.setText(receivedName);
       // imageView.setImageResource(receivedImage);
        //enable back Button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ///pfd
        downlod(intent.getStringExtra("fileName"));
    }


    private PDFView downlod(String nameFile){
        // buttonCours1Pdf.setOnClickListener(new OnClickListener(){
        //   public void onClick(View v){
//PDF View

        PDFView pdfView = findViewById(R.id.pdfview2);

        pdfView.fromAsset(nameFile)
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
                .scrollHandle(null)
                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        //}
        //});

        return pdfView;
    }

}
