package com.solinatoumi.soutienscolaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EspaceEtudiant extends AppCompatActivity {
    Button Cours;
    Button Exercices;
    Button CoursLive;
    Button Parcours;
    Button Bilan;
    Button Rappel;


    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAG","OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG","OnResume");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TAG","OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.espace_etudiant);


        Cours= (Button) findViewById(R.id.cours);
        Exercices = (Button) findViewById(R.id.exercice);
        CoursLive = (Button) findViewById(R.id.live);
        Parcours = (Button) findViewById(R.id.parcourspedagogique3);
        Bilan = (Button) findViewById(R.id.bilanapprentissage3);
        Rappel = (Button) findViewById(R.id.rappels);



        Cours.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(EspaceEtudiant.this, Cours.class);
                startActivity(intent);

            }

        });

        Exercices.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(EspaceEtudiant.this,Exercices.class);
                startActivity(intent);

            }
        });



        CoursLive.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(EspaceEtudiant.this, CoursLive.class);
                startActivity(intent);

            }

        });


        Parcours.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(EspaceEtudiant.this, Parcourspedagogique.class);
                startActivity(intent);

            }

        });

        Bilan.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(EspaceEtudiant.this, BilanApprentissage.class);
                startActivity(intent);

            }

        });

        Rappel.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(EspaceEtudiant.this, Rappels.class);
                startActivity(intent);

            }

        });




    }

    public Boolean ValidateUsername(String username) {
        boolean validate = true;

        Pattern P = Pattern.compile("(.*[a-zA-Z])");

        Matcher m = P.matcher(username);
        boolean b = m.matches();




        if (!b) {
            validate = false;//validate = false si les données ne sont pas valides, puis changer la valeur du message à envoyer au client
        }



        //Vérifier si la donnée existe déjà dans la base de données

        // if () //validate = false si les données existent déjà, puis changer la valeur du message à envoyer au client





        return validate;

    }

}



