package com.solinatoumi.soutienscolaire;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.solinatoumi.soutienscolaire.Controleur.Controleur;
import com.solinatoumi.soutienscolaire.model.User;

import java.util.ArrayList;
import java.util.List;


public class Formulaire_login_Activity<controleur> extends AppCompatActivity {


    //proprietes
     private EditText editTextEmail;
     private EditText editTextPw;
     private Button button;



    String recevedRole="";
    //ça pernet d'acceder à la base;
    Controleur controleur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulaire_connexion);
        Intent intent = getIntent();
       recevedRole =   intent.getStringExtra("role");
       Log.i("info","recevedRole**************:"+recevedRole);
        //initialisation des variables
        init();
        this.controleur = Controleur.getInstance(this);

    }

    /**
     * on associe chaque variable a son objet graphiqude de la vue grace a des id
     */
    private void init(){
        editTextEmail = findViewById(R.id.editTextEmailId);
        editTextPw= findViewById(R.id.editTextPwId);
        button = findViewById(R.id.buttonId);
        validerConnexion();
    }

private void validerConnexion(){
       button.setOnClickListener(new Button.OnClickListener(){
           public void onClick(View v){
               Log.d("message","mon button valier cliqué...role:"+recevedRole);
              // Toast.makeText(Formulaire_login_Activity.this, "valider ici", Toast.LENGTH_SHORT).show();

               String email = editTextEmail.getText().toString();
               String pw = editTextPw.getText().toString();
                if(!email.isEmpty() && !pw.isEmpty() ){
                    if( recevedRole.equals("etudiant")&& controleur.verifierUser(email)){
                        Intent intent = new Intent(Formulaire_login_Activity.this, EspaceEtudiant1_Activity.class);

                        startActivity(intent);
                    }else if( recevedRole.equals("parent")&& controleur.verifierUser(email) ) {
                        Log.i("message","ici espace parent"+recevedRole);

                    Intent intent = new Intent(Formulaire_login_Activity.this, EspaceParent.class);
                    startActivity(intent);
                }
                  else{ Toast.makeText(Formulaire_login_Activity.this, "saisie incorecte", Toast.LENGTH_SHORT).show();
               }
                }else{
                    Toast.makeText(Formulaire_login_Activity.this, "merci de renseigner votre login et mot de passe", Toast.LENGTH_SHORT).show();
                }



           }
       });
}





}
