package com.solinatoumi.soutienscolaire;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.solinatoumi.soutienscolaire.Controleur.Controleur;
import com.solinatoumi.soutienscolaire.model.User;

public class Inscription  extends AppCompatActivity {

    EditText nom , prenom, mail, motDepass;
    RadioButton roleEtud, roleParent;
    Button valider;
    Spinner spinner1;
    String recevedRole="";
    Controleur controleur;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscription_activity);
        this.controleur = Controleur.getInstance(this);
 init();
        validerConnexion();
    }
 private void  init(){
        valider = (Button) findViewById(R.id.valider);
        motDepass = (EditText) findViewById(R.id.motdepass);
        mail = (EditText) findViewById(R.id.mail);
        nom = (EditText) findViewById(R.id.nomId);
        prenom = (EditText) findViewById(R.id.prenomId);
     //roleEtud.callOnClick()
        roleEtud = (RadioButton) findViewById(R.id.radio2);
        roleParent =(RadioButton) findViewById(R.id.radio);
        // telephone = (EditText) findViewById(R.id.telephone);
 }



    private void validerConnexion() {
        valider.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Log.d("message", "mon button valier cliqué...role:" + recevedRole);
                // Toast.makeText(Formulaire_login_Activity.this, "valider ici", Toast.LENGTH_SHORT).show();

                String email = mail.getText().toString();
                String pw = motDepass.getText().toString();
                String nameValue = nom.getText().toString();
                String prenomValue = prenom.getText().toString();

                if (roleEtud.isChecked()) {
                    recevedRole = "etudiant";
                }
                if (roleParent.isChecked()) {
                    recevedRole = "parent";
                }

                Log.i("info","le role selectionéest : "+recevedRole);
                User newUser = new User(nameValue, prenomValue, recevedRole, email, pw);

                Log.i("info","le nouveau user : "+newUser);
                //avant d'ajouter un utlisateur on vérifie si il n'existe pas dans la base
                if(email.isEmpty() || pw.isEmpty() || nameValue.isEmpty() || prenomValue.isEmpty() || recevedRole.isEmpty()){
                    Toast.makeText(Inscription.this, "merci de remplir tout les champs du formulaire...", Toast.LENGTH_SHORT).show();

                }else if(controleur.verifierUser(newUser.getEmail())){
                   Toast.makeText(Inscription.this, "OOP!! le mail:"+email+" est déja associé à un autre compte", Toast.LENGTH_SHORT).show();

                }else {
                    try {
                        controleur.ajouterUser(newUser);
                        if(recevedRole.equals("etudiant")){
                            Intent intent = new Intent(Inscription.this,EspaceEtudiant1_Activity .class);
                            startActivity(intent);

                        }else{
                            Intent intent = new Intent(Inscription.this, EspaceParent.class);
                            startActivity(intent);

                        }
                    }catch(Exception e){
                        Log.i("exp","l'erreur: "+e);
                        Toast.makeText(Inscription.this, "OOP!! le mail:"+email+"et/ou mot de passe "+" sont déja associés à un autre compte", Toast.LENGTH_SHORT).show();

                    }


                }

            }
        });


    }

}