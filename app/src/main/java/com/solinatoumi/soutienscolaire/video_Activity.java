package com.solinatoumi.soutienscolaire;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;


public class video_Activity extends AppCompatActivity {

    VideoView simpleVideoView;
    MediaController mediaControls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_);
        // Find your VideoView in your video_main.xml layout
        simpleVideoView = (VideoView) findViewById(R.id.videoView2);

        if (mediaControls == null) {
            // create an object of media controller class
            mediaControls = new MediaController(video_Activity.this);
            mediaControls.setAnchorView(simpleVideoView);
        }
        // set the media controller for video view
        simpleVideoView.setMediaController(mediaControls);

        // set the uri for the video view
        //simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video1));
        Log.i("info:","ici**************************");
        //simpleVideoView.setVideoURI(Uri.parse("https://vod-progressive.akamaized.net/exp=1579404917~acl=%2A%2F344063724.mp4%2A~hmac=8668247d976d9f99fdd907be5699ba72a27a5b15fbb0a89f7f36964a7d6d6e1c/vimeo-prod-skyfire-std-us/01/4331/4/121659103/344063724.mp4"));
        simpleVideoView.setVideoURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/videoview-247213.appspot.com/o/Video.mp4?alt=media&token=d049cc83-5635-487d-ac48-1a36d5b64844"));

        // start a video
        simpleVideoView.start();

        // implement on completion listener on video view
        simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(getApplicationContext(), "Thank You... c'est fini!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
            }
        });
        simpleVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.i("info","c"+mp + "2:"+what+"3:"+extra);
                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!"+"1:"+mp + "2:"+what+"3:"+extra, Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                return false;
            }
        });


    }
}
