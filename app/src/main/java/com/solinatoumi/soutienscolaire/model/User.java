package com.solinatoumi.soutienscolaire.model;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

    private String nom;
    private String prenom;
    private String role;
    private String email;
    private String pw;
    private int niveau;

    public User() {
    }

    public User(String nom, String prenom, String role, String email, String pw, int niveau) {
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.email = email;
        this.pw = pw;
        this.niveau = niveau;
    }

    public User(String nom, String prenom, String role, String email, String pw) {
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.email = email;
        this.pw = pw;
    }

    public User(String email, String pw) {
        this.email = email;
        this.pw = pw;
    }

    public User( String email, String pw,String role) {
        this.role = role;
        this.email = email;
        this.pw = pw;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getRole().equals(user.getRole()) &&
                getEmail().equals(user.getEmail()) &&
                getPw().equals(user.getPw());
    }

    @Override
    public String toString() {
        return "User{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", role='" + role + '\'' +
                ", email='" + email + '\'' +
                ", pw='" + pw + '\'' +
                ", niveau=" + niveau +
                '}';
    }
}

