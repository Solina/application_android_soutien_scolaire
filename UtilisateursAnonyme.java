package com.solinatoumi.soutienscolaire;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilisateursAnonyme extends AppCompatActivity {

    Button NiveauScolaires ;
    Button PourquoiScolarides;
    Button NosOffres;

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAG","OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG","OnResume");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TAG","OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.utilisateurs_anonyme);

        NiveauScolaires = (Button) findViewById(R.id.niveau);
        PourquoiScolarides = (Button) findViewById(R.id. navigation);
        NosOffres = (Button) findViewById(R.id.offfres);


        NiveauScolaires.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UtilisateursAnonyme.this, NiveauScolaire.class);
                startActivity(intent);

            }
        });



        PourquoiScolarides .setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UtilisateursAnonyme.this, MainActivity.class);
                startActivity(intent);
            }
        });


        NosOffres.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UtilisateursAnonyme.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }


}

